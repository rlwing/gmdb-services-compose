#/bin/sh

docker run -d -p 8080:8080 \
           -e EUREKA_CLIENT_ENABLED=true \
           -e EUREKA_HOST=gmdb-discovery:8761 \
           -e DB_HOST=gmdb-devdb:3306 \
           -e DB_USER=gmdb_app \
           -e DB_PWD=1234 \
           --network gmdb-bridge \
           gmdb/movies
