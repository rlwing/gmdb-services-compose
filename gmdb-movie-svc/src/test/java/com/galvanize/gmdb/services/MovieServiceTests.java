package com.galvanize.gmdb.services;

import com.galvanize.gmdb.entities.Movie;
import com.galvanize.gmdb.repositories.MovieRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MovieServiceTests {
    private static final int TEST_MOVIES_COUNT = 20;
    private static final String TEST_MOVIE_TITLE_PREFIX = "GMDB JUNIT TEST-";

    private final List<Movie> moviesList = new ArrayList<>();
    private static final String[] ACTORS_1 = {"ONE", "TWO", "THREE"};
    private static final String[] ACTORS_2 = {"FOUR", "FIVE"};
    private static final String[] ACTORS_3 = {"SIX", "SEVEN", "EIGHT", "NINE"};
    private static final String DIRECTOR_1 = "DIRECTOR ONE";
    private static final String DIRECTOR_2 = "DIRECTOR TWO";
    private static final String DIRECTOR_3 = "DIRECTOR THREE";

    @Mock
    MovieRepository movieRepository;

    MovieService movieService;

    @BeforeEach
    public void setUp() {
        movieService = new MovieService(movieRepository);

        Movie m;
        int TEST_MOVIES_COUNT = 20;
        for (int i = 1; i < TEST_MOVIES_COUNT +1; i++) {
            m = new Movie();
            m.setMovieId(1000L+i);
            m.setTitle(TEST_MOVIE_TITLE_PREFIX + (i * 13));
            m.setImdbid("im" + i * 13);
            if(i%3==0){
                m.setActors(String.join(",", ACTORS_3));
                m.setDirector(DIRECTOR_3);
            }else if(i%2==0){
                m.setActors(String.join(",", ACTORS_2));
                m.setDirector(DIRECTOR_2);
            }else{
                m.setActors(String.join(",", ACTORS_1));
                m.setDirector(DIRECTOR_1);
            }

            moviesList.add(m);
        }
    }

    @Test
    public void searchMovies() {
        when(movieRepository.findMovieByTitleContains(TEST_MOVIE_TITLE_PREFIX)).thenReturn(moviesList);

        List<Movie> movies = movieService.search(TEST_MOVIE_TITLE_PREFIX);
        assertEquals(TEST_MOVIES_COUNT, movies.size());
        assertTrue(movies.get(TEST_MOVIES_COUNT /2).getTitle().startsWith(TEST_MOVIE_TITLE_PREFIX));
    }

    @Test
    public void findByImdbId() {
        when(movieRepository.findMovieByImdbid(anyString())).thenReturn(moviesList.get(13));

        Movie m = movieService.findMovieByImdbId(moviesList.get(13).getImdbid());

        assertNotNull(m);
        assertEquals(moviesList.get(13).getTitle(), m.getTitle());
        assertEquals(moviesList.get(13).getImdbid(), m.getImdbid());
        assertEquals(moviesList.get(13).getMovieId(), m.getMovieId());
    }

    @Test
    public void findRandomMovies() {
        when(movieRepository.findRandomMovies(3)).thenReturn(
                Arrays.asList(moviesList.get(2),moviesList.get(4),moviesList.get(7))
        );
        int MOVIES_TO_FIND = 3;
        List<Movie> movies = movieService.findRandomMovies(MOVIES_TO_FIND);
        assertEquals(MOVIES_TO_FIND, movies.size());

    }
}
