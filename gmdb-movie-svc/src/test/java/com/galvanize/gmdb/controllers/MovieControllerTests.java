package com.galvanize.gmdb.controllers;

import com.galvanize.gmdb.entities.Movie;
import com.galvanize.gmdb.services.MovieService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.util.AssertionErrors.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(MovieController.class)
public class MovieControllerTests {
    private static final String TEST_MOVIE_TITLE_PREFIX = "GMDB JUNIT TEST-";

    private final List<Movie> moviesList = new ArrayList<>();
    private static final String[] ACTORS_1 = {"ONE", "TWO", "THREE"};
    private static final String[] ACTORS_2 = {"FOUR", "FIVE"};
    private static final String[] ACTORS_3 = {"SIX", "SEVEN", "EIGHT", "NINE"};
    private static final String DIRECTOR_1 = "DIRECTOR ONE";
    private static final String DIRECTOR_2 = "DIRECTOR TWO";
    private static final String DIRECTOR_3 = "DIRECTOR THREE";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private MovieService movieService;

    @BeforeEach
    public void setUp() {
        Movie m;
        int TEST_MOVIES_COUNT = 20;
        for (int i = 1; i < TEST_MOVIES_COUNT +1; i++) {
            m = new Movie();
            m.setMovieId(1000L+i);
            m.setTitle(TEST_MOVIE_TITLE_PREFIX + (i * 13));
            m.setImdbid("im" + i * 13);
            if(i%3==0){
                m.setActors(String.join(",", ACTORS_3));
                m.setDirector(DIRECTOR_3);
            }else if(i%2==0){
                m.setActors(String.join(",", ACTORS_2));
                m.setDirector(DIRECTOR_2);
            }else{
                m.setActors(String.join(",", ACTORS_1));
                m.setDirector(DIRECTOR_1);
            }

            moviesList.add(m);
        }
    }

    @Test
    public void env() {
        assertNotNull("MockMvc was null", mvc);
    }

    @Test
    public void searchMovies() throws Exception{
        when(movieService.search(anyString())).thenReturn(moviesList);

        mvc.perform(get("/api/movies/?search="+TEST_MOVIE_TITLE_PREFIX))
                .andExpect(status().isOk()).andReturn();
    }

    @Test
    public void getMovieByImdbId() throws Exception{

        when(movieService.findMovieByImdbId(moviesList.get(7).getImdbid())).thenReturn(moviesList.get(7));

        mvc.perform(get("/api/movies/imdb/"+moviesList.get(7).getImdbid()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title", is(moviesList.get(7).getTitle())))
                .andExpect(jsonPath("$.movieId", is(moviesList.get(7).getMovieId().intValue())));
    }

    @Test
    public void getMovieById() throws Exception{

        when(movieService.findMovieById(anyLong())).thenReturn(moviesList.get(3));
        Movie testMovie = moviesList.get(3);

        mvc.perform(get(String.format("/api/movies/%s", testMovie.getMovieId().intValue())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.movieId", is(testMovie.getMovieId().intValue())))
                .andExpect(jsonPath("$.title", is(testMovie.getTitle())));
    }

    @Test
    public void findRandomMovies() throws Exception{
        when(movieService.findRandomMovies(2)).thenReturn(Arrays.asList(moviesList.get(2), moviesList.get(5)));

        mvc.perform(get("/api/movies/rand?qty=2"))
               .andExpect(status().isOk());
    }

    @Test
    public void testSidEndpoint() throws Exception {
        mvc.perform(get("/api/movies/sid"))
                .andExpect(status().isOk());
    }

}
