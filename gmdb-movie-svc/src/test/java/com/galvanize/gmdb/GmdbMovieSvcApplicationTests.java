package com.galvanize.gmdb;


import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest
@TestPropertySource(locations= "classpath:application-test.properties")
public class GmdbMovieSvcApplicationTests {

    @Test
    public void contextLoads() {
    }

    //FAILS in docker build
//    @Test
//    public void runMain() {
//        String[] args = new String[0];
//        GmdbMovieSvcApplication.main(args);
//    }
}
