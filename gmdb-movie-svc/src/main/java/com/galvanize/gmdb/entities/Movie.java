package com.galvanize.gmdb.entities;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "movies")
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "MOVIE_ID", unique = true, nullable = false)
    private Long movieId;

    @Column(name="METASCORE")
    private String metascore;

    @Column(name="BOXOFFICE")
    private String boxOffice;

    @Column(name="WEBSITE")
    private String website;

    @Column(name="IMDB_RATING")
    private String imdbRating;

    @Column(name="IMDB_VOTES")
    private String imdbVotes;

    @Column(name="RUNTIME")
    private String runtime;

    @Column(name="LANGUAGE")
    private String language;

    @Column(name="RATED")
    private String rated;

    @Column(name="PRODUCTION")
    private String production;

    @Column(columnDefinition = "date", name="RELEASED")
    @JsonFormat(pattern = "dd MMM yyyy")
    private Date released;

    @Column(name="IMDBID")
    private String imdbid;

    @Column(name="PLOT",columnDefinition = "LONGTEXT")
    private String plot;

    @Column(name="DIRECTOR")
    private String director;

    @Column(name="TITLE")
    private String title;

    @Column(name="ACTORS")
    private String actors;

    @Column(name="RESPONSE")
    private String response;

    @Column(name="TYPE")
    private String type;

    @Column(name="AWARDS")
    private String awards;

    @Column(columnDefinition = "date", name="DVD")
    @JsonFormat(pattern = "dd MMM yyyy")
    private Date dvd;

    @Column(name="YEAR")
    private String year;

    @Column(name="POSTER")
    private String poster;

    @Column(name="COUNTRY")
    private String country;

    @Column(name="GENRE")
    private String genre;

    @Column(name="WRITER",columnDefinition = "LONGTEXT")
    private String writer;

    public Movie() {
    }

    public Long getMovieId() {
        return movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    public String getMetascore() {
        return metascore;
    }

    public void setMetascore(String metascore) {
        this.metascore = metascore;
    }

    public String getBoxOffice() {
        return boxOffice;
    }

    public void setBoxOffice(String boxOffice) {
        this.boxOffice = boxOffice;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getImdbRating() {
        return imdbRating;
    }

    public void setImdbRating(String imdbRating) {
        this.imdbRating = imdbRating;
    }

    public String getImdbVotes() {
        return imdbVotes;
    }

    public void setImdbVotes(String imdbVotes) {
        this.imdbVotes = imdbVotes;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getRated() {
        return rated;
    }

    public void setRated(String rated) {
        this.rated = rated;
    }

    public String getProduction() {
        return production;
    }

    public void setProduction(String production) {
        this.production = production;
    }

    public Date getReleased() {
        return released;
    }

    public void setReleased(Date released) {
        this.released = released;
    }

    public String getImdbid() {
        return imdbid;
    }

    public void setImdbid(String imdbid) {
        this.imdbid = imdbid;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        actors = actors;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAwards() {
        return awards;
    }

    public void setAwards(String awards) {
        this.awards = awards;
    }

    public Date getDvd() {
        return dvd;
    }

    public void setDvd(Date DVD) {
        this.dvd = DVD;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "movieId=" + movieId +
                ", metascore='" + metascore + '\'' +
                ", boxOffice='" + boxOffice + '\'' +
                ", website='" + website + '\'' +
                ", imdbRating='" + imdbRating + '\'' +
                ", imdbVotes='" + imdbVotes + '\'' +
                ", runtime='" + runtime + '\'' +
                ", language='" + language + '\'' +
                ", rated='" + rated + '\'' +
                ", production='" + production + '\'' +
                ", released=" + released +
                ", imdbid='" + imdbid + '\'' +
                ", plot='" + plot + '\'' +
                ", director='" + director + '\'' +
                ", title='" + title + '\'' +
                ", actors='" + actors + '\'' +
                ", response='" + response + '\'' +
                ", type='" + type + '\'' +
                ", awards='" + awards + '\'' +
                ", dvd=" + dvd +
                ", year='" + year + '\'' +
                ", poster='" + poster + '\'' +
                ", country='" + country + '\'' +
                ", genre='" + genre + '\'' +
                ", writer='" + writer + '\'' +
                '}';
    }
}
