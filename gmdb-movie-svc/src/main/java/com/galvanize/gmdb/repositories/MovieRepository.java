package com.galvanize.gmdb.repositories;

import com.galvanize.gmdb.entities.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {
    List<Movie> findMovieByTitleContains(String criteria);

    Movie findMovieByImdbid(String imdbid);

    @Query(value = "SELECT * from movies m order by RAND() LIMIT ?1", nativeQuery = true)
    List<Movie> findRandomMovies(int qty);

    @Query(value = "select title, actors, genre from movies where actors like '?' and genre like '?'", nativeQuery = true)
    List<Movie> findStuff(String actor, String genre);

    List<Movie> findMoviesByActorsContainsAndGenreContains(String actors, String genre);

    @Query(value = "SELECT * from movies m where title like '?' and actors like '?' and director like '?' and plot like '?' and genre like '?'",
            nativeQuery = true)
    List<Movie> doSearchBy(String title, String actors, String director, String plot, String genre);

    @Query(value = "select * from movies m where title like ?", nativeQuery = true)
    List<Movie> searchByTitleContains(String str );
}
