package com.galvanize.gmdb;

import com.galvanize.gmdb.controllers.MovieController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@EnableEurekaClient
public class GmdbMovieSvcApplication {

    public static void main(String[] args) {
        SpringApplication.run(GmdbMovieSvcApplication.class, args);
    }

}
