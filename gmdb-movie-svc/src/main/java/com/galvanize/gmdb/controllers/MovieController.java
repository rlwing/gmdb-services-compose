package com.galvanize.gmdb.controllers;

import com.galvanize.gmdb.entities.Movie;
import com.galvanize.gmdb.repositories.MovieRepository;
import com.galvanize.gmdb.services.MovieService;
import org.apache.commons.lang.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/movies")
public class MovieController {

    private MovieService service;

    @Value("${app.server.id}")
    private int serverId;

    @Autowired
    public MovieController(MovieService service) {
        this.service = service;
    }

    @GetMapping("/sid")
    public String getServerId(){
        return "The server id is '"+serverId+"'";
    }

    @GetMapping
    public List<Movie> searchMovies(@RequestParam(name = "search", required = false, defaultValue = "") String searchString,
                                    HttpServletRequest request){
        return service.search(searchString);
    }

    @GetMapping("/imdb/{imdbid}")
    public Movie getMovieByImdbId(@PathVariable String imdbid){
        return service.findMovieByImdbId(imdbid);
    }

    @GetMapping("/{id}")
    public Movie getMovieById(@PathVariable Long id){
        return service.findMovieById(id);
    }

    @GetMapping("/rand")
    public List<Movie> getRandomMovies(@RequestParam(required = false, defaultValue = "3") int qty){
        return service.findRandomMovies(qty);
    }

}
