package com.galvanize.gmdb;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest
@TestPropertySource(locations="classpath:test.properties")
class GmdbReviewSvcApplicationTests {

    @Test
    void contextLoads() {
    }

//    @Test
//    void testMain() {
//        GmdbReviewSvcApplication.main(new String[]{""});
//    }
}
