package com.galvanize.gmdb.services;

import com.galvanize.gmdb.entities.TokenDetails;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TokenHelperTest {

    public static String JWT_SECRET = "JwtSecretKey";

    TokenHelper tokenHelper = new TokenHelper();

    @Test
    void getTokenDetails() {
        String dummyToken = generateDummyToken("admin@gmdb.com", "admin@gmdb.com",
                                                Arrays.asList("ROLE_ADMIN", "ROLE_USER"));

        TokenDetails tokenDetails = tokenHelper.getTokenDetails(dummyToken);
        assertEquals(9999, tokenDetails.getGuid());
        assertEquals("admin@gmdb.com", tokenDetails.getEmail());
        assertEquals("admin@gmdb.com", tokenDetails.getUsername());
        List<String> roles = Arrays.asList("ROLE_ADMIN", "ROLE_USER");
        assertEquals(roles, tokenDetails.getAuthorities());
        System.out.println(tokenDetails);
    }

    public static String generateDummyToken(String userid, String email, List<String> roles){
        long now = System.currentTimeMillis();
        String token = Jwts.builder()
                .setSubject(userid)
                .claim("email", email)
                .claim("guid", 9999)
                .claim("authorities", roles)
                .setIssuedAt(new Date(now))
                .setExpiration(new java.sql.Date(now + 5 * 60 * 1000))  // 5 mins in milliseconds
                .signWith(SignatureAlgorithm.HS512, JWT_SECRET.getBytes())
                .compact();

        return "Bearer "+token;
    }

}