package com.galvanize.gmdb.services;

import com.galvanize.gmdb.entities.Review;
import com.galvanize.gmdb.entities.ReviewDto;
import com.galvanize.gmdb.repositories.ReviewRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
@Transactional
@TestPropertySource(locations="classpath:test.properties")
public class ReviewServiceTests {

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private ReviewRepository reviewRepository;

    private List<Review> testReviews;

    @MockBean
    JdbcHelper jdbcHelper;

    ReviewDto reviewDto;

    @BeforeEach
    void setUp() {
        testReviews = new ArrayList<>();
        for (long i = 40; i < 50; i++) {
            testReviews.add(new Review(i, "Review Title number "+i, "ReviewBody", i));
        }
        reviewRepository.saveAll(testReviews);

        reviewDto = new ReviewDto();
        reviewDto.setEmail("user@gmdb.com");
        reviewDto.setImdbId("imdbid22");
        reviewDto.setReviewTitle("Test Review Title");
        reviewDto.setReviewText("Test Review Text");

    }

    @Test
    void getAllReviews() {
        List<Review> reviews = reviewService.findAll();

        reviews.forEach(System.out::println);

        assertEquals(testReviews, reviews);
    }

    @Test
    void findOne() {
        Review expected = testReviews.get(7);

        Review actual = reviewService.findById(expected.getId()).getBody();

        assertNotNull(actual);
        assertEquals(expected, actual);
    }

    @Test
    void createReviewUserExists() {
        ReviewDto expected = reviewDto;
        Mockito.when(jdbcHelper.movieExistsById(22L)).thenReturn(true);
        Mockito.when(jdbcHelper.findUserIdFromEmail("user@gmdb.com")).thenReturn(Optional.of(2L));
        Mockito.when(jdbcHelper.userExistsById(2L)).thenReturn(true);
        Mockito.when(jdbcHelper.findMovieIdFromImdbId("imdbid22")).thenReturn(Optional.of(22L));
        ReviewDto actual = reviewService.createNew(reviewDto);

        assertNotNull(actual);
        assertEquals(expected, actual);
    }
    @Test()
    void createReviewUserNotExists() {

        when(jdbcHelper.userExistsById(77L)).thenReturn(false);

        assertThrows(RuntimeException.class, () -> reviewService.createNew(reviewDto));
    }

    @Test
    void findByImdbId() {
        Review expected = testReviews.get(3);
        String imdbId = "a111aa";

        when(jdbcHelper.findMovieIdFromImdbId(imdbId)).thenReturn(Optional.of(expected.getMovieId()));
        List<Review> actual = reviewService.findByImdbId(imdbId);

        assertFalse(actual.isEmpty());
        assertEquals(expected, actual.get(0));
    }

}
