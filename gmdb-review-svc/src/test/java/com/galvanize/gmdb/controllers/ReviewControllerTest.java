package com.galvanize.gmdb.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.galvanize.gmdb.entities.Review;
import com.galvanize.gmdb.entities.ReviewDto;
import com.galvanize.gmdb.repositories.ReviewRepository;
import com.galvanize.gmdb.services.JdbcHelper;
import com.galvanize.gmdb.services.TokenHelperTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest @Transactional
@AutoConfigureMockMvc
@TestPropertySource(locations="classpath:test.properties")
class ReviewControllerTest {

    @Autowired
    MockMvc mvc;

    @Autowired
    ReviewRepository reviewRepository;

    @MockBean
    JdbcHelper jdbcHelper;

    ObjectMapper mapper = new ObjectMapper();

    List<Review> testReviews;

    @BeforeEach
    void setUp() {
        testReviews = new ArrayList<>();
        for (long i = 40; i < 50; i++) {
            testReviews.add(new Review(i, "Review Title number "+i, "ReviewBody", 1L));
        }
        reviewRepository.saveAll(testReviews);

    }

    @Test
    void findAll() throws Exception {
        mvc.perform(get("/api/reviews"))
                .andExpect(status().isOk());
    }

    @Test
    void findOne() throws Exception {
        mvc.perform(get("/api/reviews/"+testReviews.get(5).getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.reviewTitle", is(testReviews.get(5).getReviewTitle())));
    }

    @Test
    void postReviewMovieExists() throws Exception {
        ReviewDto reviewDto = new ReviewDto();
        reviewDto.setEmail("user@gmdb.com");
        reviewDto.setImdbId("imdbid22");
        reviewDto.setReviewTitle("Test Review Title");
        reviewDto.setReviewText("Test Review Text");

        Mockito.when(jdbcHelper.movieExistsById(22L)).thenReturn(true);
        Mockito.when(jdbcHelper.findUserIdFromEmail("user@gmdb.com")).thenReturn(Optional.of(2L));
        Mockito.when(jdbcHelper.userExistsById(2L)).thenReturn(true);
        Mockito.when(jdbcHelper.findMovieIdFromImdbId("imdbid22")).thenReturn(Optional.of(22L));

        String token = TokenHelperTest.generateDummyToken(reviewDto.getEmail(), reviewDto.getEmail(), Arrays.asList("ROLE_USER", "ROLE_ADMIN"));

        mvc.perform(post("/api/reviews")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(reviewDto))
                        .header("Authorization", token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.reviewId", is(notNullValue())));

    }

    @Test
    void postReviewMovieNotExists() throws Exception {
        String json = "{ \"movieId\": 22, " +
                "\"reviewerId\": 22, " +
                "\"reviewTitle\": \"This is a title\" }";

        Mockito.when(jdbcHelper.movieExistsById(22L)).thenReturn(false);
        Mockito.when(jdbcHelper.userExistsById(22L)).thenReturn(true);

        mvc.perform(post("/api/reviews").contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().is(401));

    }

    @Test
    void getReviewsByImdbId() throws Exception {
        String imdbid = "aa111bb";
        Long movieId = testReviews.get(5).getMovieId();
        Mockito.when(jdbcHelper.findMovieIdFromImdbId(imdbid)).thenReturn(Optional.of(movieId));

        mvc.perform(get("/api/reviews/imdb/"+imdbid))
                .andDo(print())
                .andExpect(status().isOk());
    }
}