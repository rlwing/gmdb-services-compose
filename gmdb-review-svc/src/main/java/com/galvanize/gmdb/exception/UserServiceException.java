package com.galvanize.gmdb.exception;

public class UserServiceException extends Throwable {
    public UserServiceException(String message) {
        super(message);
    }
}
