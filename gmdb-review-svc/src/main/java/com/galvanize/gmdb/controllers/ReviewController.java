package com.galvanize.gmdb.controllers;

import com.galvanize.gmdb.entities.Review;
import com.galvanize.gmdb.entities.ReviewDto;
import com.galvanize.gmdb.entities.TokenDetails;
import com.galvanize.gmdb.services.JdbcHelper;
import com.galvanize.gmdb.services.ReviewService;
import com.galvanize.gmdb.services.TokenHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/reviews")
public class ReviewController {

    ReviewService reviewService;
    TokenHelper tokenHelper;
    JdbcHelper jdbcHelper;

    @Value("${app.server.id}")
    private int serverId;

    public ReviewController(ReviewService reviewService, TokenHelper tokenHelper, JdbcHelper jdbcHelper) {
        this.reviewService = reviewService;
        this.tokenHelper = tokenHelper;
        this.jdbcHelper = jdbcHelper;
    }

    @GetMapping("/sid")
    public String getServerId(){
        return "The server id is '"+serverId+"'";
    }

    @GetMapping
    public List<Review> getAllReviews(){
        return reviewService.findAll();
    }

    @GetMapping("{reviewId}")
    public ResponseEntity<Review> getByReviewId(@PathVariable long reviewId){
        return reviewService.findById(reviewId);
    }

    @PostMapping
    public ResponseEntity<ReviewDto> createNewReview(@RequestBody ReviewDto reviewDto, HttpServletRequest request){
        // Parse the token
        String token = request.getHeader("Authorization");
        if(token == null || !token.startsWith("Bearer")){
            return ResponseEntity.status(401).build();
        }
        TokenDetails tokenDetails = tokenHelper.getTokenDetails(token);
        // Does the token email equal the email sent in?
        if(!tokenDetails.getEmail().equals(reviewDto.getEmail())){
            return ResponseEntity.badRequest().header("errorMsg", "invalid user").build();
        }
        reviewDto.setReviewerId(Long.valueOf(tokenDetails.getGuid()));
        //Long movieId, String reviewTitle, String reviewText, Long reviewerId
        try {
            return ResponseEntity.ok(reviewService.createNew(reviewDto));
        }catch (RuntimeException re){
            return ResponseEntity.badRequest().header("errorMsg", re.getMessage()).build();
        }

    }

    @GetMapping("/imdb/{imdbid}")
    public ResponseEntity<List<Review>> getByImdbId(@PathVariable String imdbid){
        List<Review> reviews;
        try{
            reviews = reviewService.findByImdbId(imdbid);
            return ResponseEntity.ok(reviews);
        }catch (Exception ex){
            return ResponseEntity.notFound().header("message", "no reviews found for imdbid "+imdbid).build();
        }

    }
}
