package com.galvanize.gmdb.services;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class JdbcHelper {

    private JdbcTemplate jdbcTemplate;

    public JdbcHelper(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Boolean movieExistsById(long movieId) {
        return jdbcTemplate.queryForObject(
                "select exists(select 1 from movies where movie_id = ?)", Boolean.class, movieId);
    }

    public Boolean userExistsById(long userId) {
        return jdbcTemplate.queryForObject(
                "select exists(select 1 from users where id = ?)", Boolean.class, userId);
    }

    public Optional<Long> findMovieIdFromImdbId(String imdbId){
        return Optional.ofNullable(jdbcTemplate.queryForObject(
                "select movie_id from movies where imdbid = ?", Long.class, imdbId));
    }

    public Optional<Long> findUserIdFromEmail(String email) {
        return Optional.ofNullable(jdbcTemplate.queryForObject(
                "select id from users where email = ?", Long.class, email));
    }
}
