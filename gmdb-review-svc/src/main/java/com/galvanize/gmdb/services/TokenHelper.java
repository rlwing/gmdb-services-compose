package com.galvanize.gmdb.services;

import com.galvanize.gmdb.entities.TokenDetails;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TokenHelper {

    public TokenDetails getTokenDetails(String token){

        if(token.startsWith("Bearer"))
            token = token.replace("Bearer", "");

        Claims claims = getTokenClaims(token);
        TokenDetails details = new TokenDetails();
        details.setGuid((Integer)claims.get("guid"));
        details.setUsername(claims.getSubject());
        details.setEmail((String)claims.get("email"));
        List<String> authorities = (List<String>) claims.get("authorities");
        details.setAuthorities(authorities);

        return details;
    }

    private Claims getTokenClaims(String token) {

        return Jwts.parser()
                .setSigningKey("JwtSecretKey".getBytes())
                .parseClaimsJws(token)
                .getBody();

    }
}
