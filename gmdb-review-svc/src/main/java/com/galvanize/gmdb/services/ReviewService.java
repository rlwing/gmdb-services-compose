package com.galvanize.gmdb.services;

import com.galvanize.gmdb.entities.Review;
import com.galvanize.gmdb.entities.ReviewDto;
import com.galvanize.gmdb.repositories.ReviewRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ReviewService {

    ReviewRepository reviewRepository;
    JdbcHelper jdbcHelper;

    public ReviewService(ReviewRepository reviewRepository, JdbcHelper jdbcHelper) {
        this.reviewRepository = reviewRepository;
        this.jdbcHelper = jdbcHelper;
    }

    public List<Review> findAll() {
        return reviewRepository.findAll();
    }

    public ResponseEntity<Review> findById(Long id) {
        Optional<Review> optionalReview = reviewRepository.findById(id);
        return optionalReview.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    public ReviewDto createNew(ReviewDto reviewDto) {
        //"Long movieId, String reviewTitle, String reviewText, Long reviewerId"
        Optional<Long> oMovieId = jdbcHelper.findMovieIdFromImdbId(reviewDto.getImdbId());
        Optional<Long> oReviewerId = jdbcHelper.findUserIdFromEmail(reviewDto.getEmail());
        if(!oMovieId.isPresent() || !oReviewerId.isPresent()){
            throw new RuntimeException("Movie or Reviewer does not exist");
        }
        Review review = new Review(oMovieId.get(),
                                   reviewDto.getReviewTitle(),
                                   reviewDto.getReviewText(),
                                   oReviewerId.get());

        if (jdbcHelper.movieExistsById(review.getMovieId()) && jdbcHelper.userExistsById(review.getReviewerId())){
            review = reviewRepository.save(review);
            reviewDto.setReviewId(review.getId());
            return reviewDto;
        }else{
            throw new RuntimeException(String.format("User ID %s and/or Movie ID %s does not exist",
                                                     review.getReviewerId(), review.getMovieId()));
        }
    }

    public List<Review> findByImdbId(String imdbId) {
        Optional<Long> movieId = jdbcHelper.findMovieIdFromImdbId(imdbId);
        if(!movieId.isPresent()){
            return new ArrayList<>();
        }
        return reviewRepository.findAllByMovieId(movieId.get());
    }
}
