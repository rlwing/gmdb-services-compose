package com.galvanize.gmdb.entities;

public enum RoleName {
    ROLE_USER,
    ROLE_PM,
    ROLE_ADMIN
}
