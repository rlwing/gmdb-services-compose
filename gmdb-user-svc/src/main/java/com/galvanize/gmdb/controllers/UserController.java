package com.galvanize.gmdb.controllers;

import com.galvanize.gmdb.entities.User;
import com.galvanize.gmdb.exception.UserServiceException;
import com.galvanize.gmdb.rest.UpdatePasswordRequest;
import com.galvanize.gmdb.services.UserService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/users")
public class UserController {

    private UserService service;

    @Autowired
    public UserController(UserService service) {
        this.service = service;
    }

    @GetMapping("/{userid}")
    public User getUserById(@PathVariable Long userid) {
        return service.getUser(userid);
    }

    @GetMapping
    public ResponseEntity<User> getCurrentUser(HttpServletRequest httpServletRequest){
        String token = httpServletRequest.getHeader("Authorization");
        if (token == null || !token.startsWith("Bearer")){
            return ResponseEntity.status(401).header("errorMsg", "Token missing").build();
        }
        String email = getUserIdFromToken(token);
        if (email == null){
            return ResponseEntity.status(500).header("errorMsg", "email not found in token").build();
        }

        Optional<User> user = service.getUser(email);

        return user.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.status(500)
                .header("errorMsg", "Could not retrieve User object for email "+email).build());

    }

    private String getUserIdFromToken(String token){
        if(token.startsWith("Bearer")) token = token.replace("Bearer ", "");
        Claims claims = Jwts.parser()
                .setSigningKey("JwtSecretKey".getBytes())
                .parseClaimsJws(token)
                .getBody();
        return (String)claims.get("email");

    }

}
