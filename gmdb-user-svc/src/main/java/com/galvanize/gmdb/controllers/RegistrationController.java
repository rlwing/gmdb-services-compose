package com.galvanize.gmdb.controllers;

import com.galvanize.gmdb.dto.ChangePwRequest;
import com.galvanize.gmdb.dto.RegistrationRequest;
import com.galvanize.gmdb.entities.User;
import com.galvanize.gmdb.exception.UserServiceException;
import com.galvanize.gmdb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegistrationController {
    @Autowired
    UserService userService;

    @PostMapping("/register")
    public ResponseEntity<String> registerUser(@RequestBody RegistrationRequest registrationRequest){
        User user;
        try {
            user = userService.registerNewUser(new User(registrationRequest));
        } catch (UserServiceException e) {
            return ResponseEntity.badRequest().header("errorMsg", e.getMessage()).build();
        }
        if (user.getId() != null){
            return ResponseEntity.ok("User "+registrationRequest.getEmail()+" registered");
        }
        return ResponseEntity.badRequest().header("errorMsg", "User not created").build();
    }

    @PostMapping("/password")
    public ResponseEntity<String> changePassword(@RequestBody ChangePwRequest changePwRequest){
        try {
            userService.updatePassword(changePwRequest.getEmail(), changePwRequest.getOldPassword(),
                    changePwRequest.getNewPassword());
            return ResponseEntity.ok("Password changed for "+changePwRequest.getEmail());
        } catch (UserServiceException e) {
            return ResponseEntity.badRequest().header("errorMsg", e.getMessage()).build();
        }
    }
}
