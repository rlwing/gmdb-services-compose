package com.galvanize.gmdb.services;

import com.galvanize.gmdb.entities.Role;
import com.galvanize.gmdb.entities.RoleName;
import com.galvanize.gmdb.entities.User;
import com.galvanize.gmdb.exception.UserServiceException;
import com.galvanize.gmdb.repositories.RoleRepository;
import com.galvanize.gmdb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.regex.Pattern;

@Service
public class UserService {

    private UserRepository userRepository;
    private PasswordEncoder encoder;
    private RoleRepository roleRepository;

    @Autowired
    public UserService(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.encoder = encoder;
    }

    public User registerNewUser(User user) throws UserServiceException {
        //Check to see if email already exists
        if (userRepository.findByEmail(user.getEmail()).isPresent()){
            throw new UserServiceException("User already exists");
        }
        if (user.getPassword() != null && !isEncrypted(user.getPassword()))
            user.setPassword(encoder.encode(user.getPassword()));

        //A new user starts out in the "USER" role
        Role role = roleRepository.findByName(RoleName.ROLE_USER);
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        user.setRoles(roles);


        return userRepository.save(user);
    }

    public User getUser(Long userid){
        Optional<User> user = userRepository.findById(userid);
        return user.orElse(null);
    }
    public Optional<User> getUser(String email){
        return userRepository.findByEmail(email);
    }


//    public List<User> getAllUsers() {
//        return userRepository.findAll();
//    }

    public User updatePassword(String email, String oldPassword, String newPassword) throws UserServiceException {
        Optional<User> user = userRepository.findByEmail(email);
        if(!user.isPresent()){
            throw new UserServiceException("User "+email+" not found");
        }else{
            return updatePassword(user.get().getId(), oldPassword, newPassword);
        }
    }

    public User updatePassword(long id, String oldPassword, String newPassword) throws UserServiceException {
        Optional<User> oUser = userRepository.findById(id);
        if(!oUser.isPresent())
            throw new UserServiceException("User not found");
        User user = oUser.get();
        if(encoder.matches(oldPassword, oUser.get().getPassword())){
            user.setPassword(encoder.encode(newPassword));
            userRepository.save(user);
            return user;
        }else{
            //TODO: Fix this!
            throw new UserServiceException("Old password doesn't match");
        }
    }

    private boolean isEncrypted(String password) {
        Pattern BCRYPT_PATTERN = Pattern.compile("\\A\\$2a?\\$\\d\\d\\$[./0-9A-Za-z]{53}");

        return BCRYPT_PATTERN.matcher(password).matches();
    }

    public boolean validateUser(String email, String password) {
        Optional<User> user = userRepository.findByEmail(email);
        return user.filter(value -> encoder.matches(password, value.getPassword())).isPresent();
    }


}
