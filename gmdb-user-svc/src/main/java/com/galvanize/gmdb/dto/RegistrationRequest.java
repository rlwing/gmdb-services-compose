package com.galvanize.gmdb.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RegistrationRequest {
    @JsonProperty(required = true)
    String email;
    @JsonProperty(required = true)
    String firstName;
    @JsonProperty(required = true)
    String lastName;
    @JsonProperty(required = true)
    String password;

    public RegistrationRequest() {
    }

    public RegistrationRequest(String email, String firstName, String lastName, String password) {
        setEmail(email);
        setFirstName(firstName);
        setLastName(lastName);
        setPassword(password);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
