package com.galvanize.gmdb.dto;

public class ChangePwRequest {
    String email;
    String oldPassword;
    String newPassword;

    public ChangePwRequest() {
    }

    public ChangePwRequest(String email, String oldPassword, String newPassword) {
        setEmail(email);
        setOldPassword(oldPassword);
        setNewPassword(newPassword);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
