package com.galvanize.gmdb.exception;

public class UserServiceException extends Exception {
    public UserServiceException(String message) {
        super(message);
    }

}
