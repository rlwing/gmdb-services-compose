package com.galvanize.gmdb.controllers;

import com.galvanize.gmdb.entities.User;
import com.galvanize.gmdb.exception.UserServiceException;
import com.galvanize.gmdb.repositories.UserRepository;
import com.galvanize.gmdb.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.transaction.Transactional;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)

@SpringBootTest
@Transactional
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UserControllerTests {
    @Autowired
    UserService service;

    @Autowired
    MockMvc mvc;

    @Autowired
    UserRepository userRepository;

    private String BASE_URI="/api/users";
    private User testUser;

    @Before
    public void setup() throws UserServiceException {
        testUser  = new User("me@email.com", "first-name", "last-name", "password");
        service.registerNewUser(testUser);
        assertNotNull(testUser.getId());
    }

    @Test
    public void findUserById() throws Exception {
        mvc.perform(get(BASE_URI+"/"+testUser.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", is(testUser.getId().intValue())));
    }

//    @Test
//    public void findCurrentUser() throws Exception {
//        User userUser = new User("user@gmdb.com", "user", "user", "password");
//        service.registerNewUser(userUser);
//
//        String userAtGmdbToken = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyQGdtZGIuY29tIiwiZW1haWwiOiJ1c2VyQGdtZGIuY29tIiwiZ3VpZCI6MiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9VU0VSIl0sImlhdCI6MTU4MzIwMzYxNSwiZXhwIjoxNTgzMjkwMDE1fQ.nhUz-pznQ028CvrK4pnhhC3YjGhoGlco_XTGVLzEDKRCWGEOyiUMBeuYRlVmz_kRsxGpfR3J0mJewkJaw4Efdw";
//        mvc.perform(get(BASE_URI).header("Authorization", userAtGmdbToken))
//                .andExpect(status().isOk())
//                .andDo(print());
//    }
}
