package com.galvanize.gmdb.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.galvanize.gmdb.dto.ChangePwRequest;
import com.galvanize.gmdb.dto.RegistrationRequest;
import com.galvanize.gmdb.entities.User;
import com.galvanize.gmdb.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class RegistrationControllerTests {

    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    MockMvc mvc;

    @Autowired
    UserService userService;

    RegistrationRequest registrationRequest;

    @Before
    public void setUp() {
        registrationRequest = new RegistrationRequest("me@gmail.com", "first name",
                "lastname", "password");
    }

    @Test
    public void registerNewUser() throws Exception {
        //Setup
        String json = mapper.writeValueAsString(registrationRequest);

        //Execute & Assert
        mvc.perform(post("/register").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(registrationRequest)))
                .andExpect(status().isOk());
    }

    @Test
    public void registerNewUser_emailExists() throws Exception {
        //Setup
        userService.registerNewUser(new User(registrationRequest));

        //Execute & Assert
        mvc.perform(post("/register").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(registrationRequest)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void changePassword() throws Exception {
        //Setup
        userService.registerNewUser(new User(registrationRequest));
        ChangePwRequest cpr = new ChangePwRequest(registrationRequest.getEmail(), registrationRequest.getPassword(),
                registrationRequest.getPassword()+"!new");

        mvc.perform(post("/password").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(cpr)))
                .andExpect(status().isOk());

        assertFalse(userService.validateUser(cpr.getEmail(), cpr.getOldPassword()));
        assertTrue(userService.validateUser(cpr.getEmail(), cpr.getNewPassword()));
    }

    @Test
    public void changePassword_badOldPassword() throws Exception {
        userService.registerNewUser(new User(registrationRequest));
        ChangePwRequest cpr = new ChangePwRequest(registrationRequest.getEmail(), registrationRequest.getPassword()+"BAD",
                registrationRequest.getPassword()+"!new");

        mvc.perform(post("/password").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(cpr)))
                .andExpect(status().isBadRequest());

        assertTrue(userService.validateUser(registrationRequest.getEmail(), registrationRequest.getPassword()));
    }

    @Test
    public void changePassword_userDoesntExist() throws Exception {
        ChangePwRequest cpr = new ChangePwRequest("email@nowhere.com", "OLDPWD", "NEWPWD");

        mvc.perform(post("/password").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(cpr)))
                .andExpect(status().isBadRequest());

    }
}
