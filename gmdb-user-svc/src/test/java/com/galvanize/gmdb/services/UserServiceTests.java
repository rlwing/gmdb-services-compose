package com.galvanize.gmdb.services;

import com.galvanize.gmdb.entities.User;
import com.galvanize.gmdb.exception.UserServiceException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.Optional;

import static org.junit.Assert.*;

@SpringBootTest
@Transactional
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class UserServiceTests {

    @Autowired
    UserService service;

    private User testUser;

    @Before
    public void setup() throws UserServiceException {
        testUser  = new User("me@email.com", "first-name", "last-name", "password");
        service.registerNewUser(testUser);
    }

    @Test
    public void getUserById(){
        User user = service.getUser(testUser.getId());
        assertNotNull(user);
        assertEquals(testUser.getId(), user.getId());
    }

    @Test
    public void getUserById_badid() {
        User user = service.getUser(99999999L);
        assertNull(user);
    }

    @Test
    public void getUserByEmail() throws UserServiceException {
        User userUser = new User("user@gmdb.com", "user", "user", "password");
        service.registerNewUser(userUser);
        Optional<User> user = service.getUser("user@gmdb.com");
        assertTrue(user.isPresent());
        System.out.println(user);

    }
}
